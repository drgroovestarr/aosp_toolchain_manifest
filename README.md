
AOSP TOOLCHAIN
==============


To initialize your local repository using the tree, use this command:


	
repo init -u https://gitlab.com/drgroovestarr/aosp_toolchain_manifest.git -b master



Then sync up with this command:

	repo sync

